import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSort, MatTableDataSource, MatPaginator} from '@angular/material';
import { ZohoService } from 'src/app/_service/zoho.service';
import { ConnectionService } from 'src/app/_services/connection.service';
import { InvoiceData } from 'src/app/_models/Interfaces/invoice-data';

@Component({
  selector: 'app-fourth-step-connection',
  templateUrl: './fourth-step-connection.component.html',
  styleUrls: ['./fourth-step-connection.component.css']
})
export class FourthStepConnectionComponent implements OnInit {
  public gridData: any = []; // zohoDetail;

  constructor(private zohoService: ZohoService, private connectionservice: ConnectionService) { }
  displayedColumns: string[] = ['id', 'invoice_id', 'customer_name', 'email',
  'invoice_date', 'currency_code', 'total', 'status', 'billing_address_city',
  'billing_address_state', 'billing_address_zip', 'billing_address_country',
  'shipping_address_street', 'shipping_address_city', 'shipping_address_state',
  'shipping_address_zip', 'shipping_address_country' ];
  dataSource = new MatTableDataSource();
  displaySection = 'message';  // loader | message  | empty | data
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.zohoService.callInvoiceList.subscribe(() => {
      this.displaySection = 'loader';
      this.zohoService.getInvoice(this.zohoService.selectedConnection.id, this.connectionservice.mappingId)
        .subscribe((data: InvoiceData) => {
          
          this.displaySection = 'data';
          this.zohoService.isInvoiceLoaded = true;
          this.gridData = data;
          console.log(data);
        });
    });
  }

  getDisplaySection(section) {
    return this.displaySection === section;
  }
}
