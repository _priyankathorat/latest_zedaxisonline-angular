import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MatDialogRef, MatRadioChange } from '@angular/material';
import { organizations, SaveConnection, ZohoOrganizationDetails } from 'src/app/_models/Interfaces/ZohoOrganizationDetails';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { ZohoService } from 'src/app/_services/zoho.service';
import { ZohoService as XohoService } from 'src/app/_service/zoho.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-zoho-connection-popup',
  templateUrl: './zoho-connection-popup.component.html',
  styleUrls: ['./zoho-connection-popup.component.css']
})
export class ZohoConnectionPopupComponent implements OnInit {
  // organisation: string[] = ['org1', 'org2', 'org3', 'org4', 'org5'];
  inputValue: string;
  showOrganizations = false;
  invalidAuthToken = false;
  invalidConnectionName = false;
  authToken: string;
  zohoOrganizations: organizations[];
  saveConnection: SaveConnection = {
    connectionName: '',
    authToken: '',
    organizationId: '',
    organizationName: ''
  };
  responseErrorMessage = '';
  constructor(
    public dialogRef: MatDialogRef<ZohoConnectionPopupComponent>,
    public zohoService: ZohoService,
    private alertify: AlertifyService,
    private _zohoService: XohoService
  ) {}
  ngOnInit() {}

  testAuthToken() {
    this.saveConnection.connectionName = this.saveConnection.connectionName.trim();
    this.saveConnection.authToken = this.saveConnection.authToken.trim();
    this._zohoService
      .getOrgnizationDetails(this.saveConnection.authToken)
      .subscribe((response: ZohoOrganizationDetails) => {
        console.log(response);
        if (!response.isValid) {
          this.ClearOrganizationList();
          this.invalidAuthToken = true;
          this.responseErrorMessage = response.message;
        } else {
          this.responseErrorMessage = '';
          this.zohoOrganizations = response.organizations;
          this.showOrganizations = true;
          this.invalidAuthToken = false;
        }
      }, error => (console.log(error)));
  }
  AuthTokenValueChange() {
    this.ClearOrganizationList();
    this.invalidAuthToken = false;
  }
  ClearOrganizationList() {
    this.showOrganizations = false;
    this.zohoOrganizations = [];
    this.saveConnection.organizationId = '';
    this.saveConnection.organizationName = null;
  }

  radioChange($event: MatRadioChange) {
    if ($event.value != null) {
      this.saveConnection.organizationId = $event.value;
      this.saveConnection.organizationName = this.zohoOrganizations.find(
        x => x.id === this.saveConnection.organizationId
      ).name;
    }
  }
  onSaveConnection() {
    console.log(this.saveConnection.organizationId);
    if (this.saveConnection.connectionName.trim() === '') {
      this.invalidConnectionName = true;
      return;
    } else {
      this._zohoService
        .saveConnection(this.saveConnection)
        .subscribe(() => {
          this.alertify.success('Zoho connection added successfully');
          this.dialogRef.close();
        });
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  enableSaveButton() {
    return this.saveConnection.organizationId !== '';
  }
}
