import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ZohoService } from 'src/app/_service/zoho.service';
import { AlertifyService } from 'src/app/_services/alertify.service';


@Component({
  selector: 'app-webhook-popup',
  templateUrl: './webhook-popup.component.html',
  styleUrls: ['./webhook-popup.component.css']
})
export class WebhookPopupComponent implements OnInit {
  // webHookUniqUrl = 'https://axis.zed-systems.com/zs-listener/';
  webHookUniqUrl = 'https://staging.zed-systems.com/zs-listener/';
  showCopiedMsg = false;
  constructor( public dialogRef: MatDialogRef<WebhookPopupComponent>,
     private zohoService: ZohoService,
     private alertify: AlertifyService) {}

  ngOnInit() {
    this.getGuid();
  }

  getGuid() {
    this.zohoService.getGuidForWebHook().subscribe((x: any) => {
      this.webHookUniqUrl += x.guid;
    });
  }

  copyInputMessage(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    this.showCopiedMessage();
    inputElement.setSelectionRange(0, 0);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onSaveClick(): void {
    this.dialogRef.close({webhookUrl: this.webHookUniqUrl});
  }

  showCopiedMessage() {
    this.showCopiedMsg = true;
    setTimeout(() => {
      this.showCopiedMsg = false;
    }, 4000);
  }
}
