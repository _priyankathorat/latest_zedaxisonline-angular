import { Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatRadioChange, MatStepper } from '@angular/material';
import { AllConnectionsResponse } from 'src/app/_models/Interfaces/AmazonResponse';
import { QBOTransTypes } from 'src/app/_models/Interfaces/QBOTransTypes';
import { ConTransTypes } from 'src/app/_models/Interfaces/ConTransTypes';
import { ConnectionService } from 'src/app/_services/connection.service';
import { ConnectionService as ConService } from 'src/app/_service/connection.service';
import { ZohoService } from 'src/app/_service/zoho.service';
import { MatDialog } from '@angular/material';
import { ZohoConnectionPopupComponent } from './zoho-connection-popup/zoho-connection-popup.component';
import { WebhookPopupComponent } from './webhook-popup/webhook-popup.component';
import { ZohoWebHookDetails } from 'src/app/_models/Interfaces/ZohoOrganizationDetails';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { ImportSummaryPopupComponent } from './import-summary-popup/import-summary-popup.component';
import { QuickbookService } from 'src/app/_service/quickbook.service';
import { MappingService } from 'src/app/_service/mapping.service';
import { AmazonService } from 'src/app/_service/amazon.service';
import { ItemSettingComponent } from './item-setting/item-setting.component';

@Component({
  selector: 'app-connection-form',
  templateUrl: './connection-form.component.html',
  styleUrls: ['./connection-form.component.css']
})
export class ConnectionFormComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;
  selectedConnection: AllConnectionsResponse = {};
  connectionImage = '';
  model: any = {};
  isQboNotConnectedError = false;
  mappingModel: any = {};
  mappingNameModel: any = {};
  isLinear = false;
  showGrid = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  amazonUserId: string;
  paypalUserId: string;
  amazonConnection = false;
  paypalconnection = false;
  AllConnectionsResponse: AllConnectionsResponse[];
  qboTransTypes: QBOTransTypes[];
  conTransType: ConTransTypes[];
  qboTransId: number;
  conTransId: number;
  MappingName = '';
  ValidMappingName = true;
  constructor(
    private _formBuilder: FormBuilder,
    private _renderer2: Renderer2,
    private connectionservice: ConnectionService,
    private zohoService: ZohoService,
    public dialog: MatDialog,
    private alertify: AlertifyService,
    private _ConnectionService: ConService,
    private quickbookService: QuickbookService,
    private mappingService: MappingService,
    private amazonService: AmazonService
  ) {}

  ngOnInit() {
    if (this.connectionservice.amazonUserid != null) {
      this.amazonConnection = true;
      this.amazonUserId = this.connectionservice.amazonUserid;
    }
    if (this.connectionservice.paypalUserId != null) {
      this.paypalconnection = true;
      this.paypalUserId = this.connectionservice.paypalUserId;
    }
    this.firstFormGroup = this._formBuilder.group({
      // firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      // secondCtrl: ['', Validators.required]
    });
    this.GetAllConnections();
    this.GetQuickbookConnection();
  }

  onStepperChange(no: number) {
    this.stepper.selectedIndex = no;
  }

  GetAllConnections() {
    this._ConnectionService.getAllConnections().subscribe(
      (data: AllConnectionsResponse[]) => {
         this.AllConnectionsResponse  = data;
      },
      error => {
        console.log(error.error);
      }
    );
  }

  GetQuickbookConnection() {
    this.quickbookService.getQuickbookConnectionDetails().subscribe(
      (response: any) => {
        if (response) {
          this.isQboNotConnectedError = false;
        } else {
          this.isQboNotConnectedError = true;
        }
      }, error => (console.log(error))
    );
  }

  radioChange($event: MatRadioChange) {
    console.log(this.selectedConnection.connectionTypeId);
      this.connectionservice.ConnectionTypesId = this.selectedConnection.connectionTypeId;
      this.connectionservice.ConnectionUserId = this.selectedConnection.id.toString();
      this.zohoService.selectedConnection = this.selectedConnection;
      this.updateConnectedImage();
      this.GetConnectionTransTypes();
  }

  changeTransType(value) {
    this.qboTransId = value.id;
    this.connectionservice.transSelectedValue = value.name;
    this.connectionservice.QBOTransTypeId = value.id;
    this.showGrid = false;
    this.GetDataForMappping();
  }

  changeConType(value) {
    this.conTransId = value.id;
    this.connectionservice.connectionTypeId = value.id;
    this.connectionservice.conSelectedValue = value.transType;
    this.showGrid = false;
    this.GetConColumnList();
  }

  GetConColumnList(): any {
    this.model.ConTransTypeId = this.conTransId;
    this.connectionservice.connectionTransTypeId = this.model.ConTransTypeId;

    this.mappingService.getConnectionTransactionColumn(this.conTransId)
      .subscribe((data: []) => {
        console.log(data);
        this.connectionservice.conColList = data;
        this.GetDataForMappping();
      });

    // this.connectionservice.GetConColumnList(this.model).subscribe(
    //   data => {
    //     this.GetDataForMappping();
    //   },
    //   error => {
    //     console.log(error.error);
    //   }
    // );
  }

  GetDataForMappping(): any {
    if (this.qboTransId) {
      this.model.QboTransId = this.qboTransId;
      this.mappingService.getDataForMappping(this.model)
        .subscribe((data: any) => {
          console.log(data);
          this.connectionservice.gridData = data;
          if (this.qboTransId != null && this.conTransId != null) {
            this.showGrid = true;
          }
        });

      // this.connectionservice.GetDataForMappping(this.model).subscribe(
      //   data => {
      //     if (this.qboTransId != null && this.conTransId != null) {
      //       this.showGrid = true;
      //     } else {
      //     }
      //   },
      //   error => {
      //     console.log(error.error);
      //   }
      // );
    }
  }

  AmazonAuth() {
    console.log(this.model);
    this.amazonService.getAmazonConnectUrl().subscribe(
      url => (window.location.href = url)
    );
  }

  ZohoAuth(): void {
    const dialogRef = this.dialog.open(ZohoConnectionPopupComponent, {
      width: '650px',
    });

    dialogRef.afterClosed().subscribe(result => {
    this.GetAllConnections();
    });
  }

  GetQBOTransTypes() {
    this.mappingService.getQuickbookTransactionTypes()
      .subscribe((data: QBOTransTypes[]) => {
        this.qboTransTypes = data;
      });
  }

  GetConnectionTransTypes() {
    this.model.ConnectionTypesId = this.selectedConnection.connectionTypeId;
    this.connectionservice.connectionTypeId = this.model.ConnectionTypesId;
    this.mappingService.getConnectionTransactionTypes(this.selectedConnection.connectionTypeId)
      .subscribe((x: ConTransTypes[] ) => {
         this.conTransType = x;
         this.GetQBOTransTypes();
      });
  }

  CheckMappingName() {
    this.mappingNameModel.userId = localStorage.getItem('userId');
    if (this.MappingName !== '') {
      this.mappingNameModel.MappingName = this.MappingName;

      this.mappingService.validateMappingName(this.mappingNameModel.MappingName)
        .subscribe(result => {
          console.log(typeof(result));
          this.ValidMappingName =  result as boolean;
          this.connectionservice.ValidMappingName = result as boolean;
          this.connectionservice.MappingName = this.mappingNameModel.MappingName;
        });

      // this.connectionservice
      //   .CheckMappingName(this.mappingNameModel)
      //   .subscribe(data => {
      //     console.log(typeof(data));
      //     this.connectionservice.isMappingNameValid = data.isMappingNameValid;
      //     // this.isMappingNameValid = response.isMappingNameValid
      //     this.ValidMappingName = data;
      //     this.connectionservice.ValidMappingName = data;
      //     this.connectionservice.MappingName = this.mappingNameModel.MappingName;
      //   });
    } else {
      this.connectionservice.ValidMappingName = true;
      this.ValidMappingName = true;
    }
  }

  CallThirdStep() {
    if (this.selectedConnection.connectionTypeId !== 4) {
      this.stepper.next();
    } else {
      const dialogRef = this.dialog.open(WebhookPopupComponent, {
        width: '650px',
      });
       dialogRef.afterClosed().subscribe((data: any) => {
         if (data) {
           this.SaveWebhookEvent(data.webhookUrl);
           this.stepper.next();
         }
       });
    }
  }

  CallSecondStep() {
    if (this.isQboNotConnectedError) {
      this.alertify.error('Please connect to Quickbook');
    } else {
      this.stepper.next();
    }
  }

  updateConnectedImage () {
    switch (this.selectedConnection.connectionTypeId) {
      case 1:
         this.connectionImage = '../../assets/images/amazon-logo.png';
        break;
      case 2:
        this.connectionImage = '../../assets/images/paypal-logo.png';
      break;
      case 4:
        this.connectionImage = '../../assets/images/zoho-header-logo.png';
      break;
      default:
        this.connectionImage = '';
        break;
    }
  }

  SaveWebhookEvent(webhookUrl: string) {
    const webHookDetails: ZohoWebHookDetails = {
      connectionTypeId: this.selectedConnection.connectionTypeId ,
      id: this.selectedConnection.id,
      webhookUrl: webhookUrl
    };
    this.zohoService.saveWebHook(webHookDetails).subscribe(x => {
      this.alertify.success('Webhook saved succesfully');
    });
  }
  ImportSummary() {
    const dialogRef = this.dialog.open(ImportSummaryPopupComponent, {
      width: '950px',
    });
     dialogRef.afterClosed().subscribe();
  }

  isInvoiceLoaded() {
    return this.zohoService.isInvoiceLoaded;
  }
}
