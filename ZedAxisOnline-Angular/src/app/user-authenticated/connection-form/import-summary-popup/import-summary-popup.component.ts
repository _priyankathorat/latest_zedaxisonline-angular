import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ConnectionService } from 'src/app/_services/connection.service';
import { ZohoService } from 'src/app/_service/zoho.service';
import { QboInoviceSummary } from 'src/app/_models/Interfaces/qbo-inovice-summary';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { QuickbookService } from 'src/app/_service/quickbook.service';

@Component({
  selector: 'app-import-summary-popup',
  templateUrl: './import-summary-popup.component.html',
  styleUrls: ['./import-summary-popup.component.css']
})
export class ImportSummaryPopupComponent implements OnInit {
  displaySection = 'loader'; // data
  showImportButton = false;
  public gridData: any = []; // zohoDetail;


  constructor(public dialogRef: MatDialogRef<ImportSummaryPopupComponent>,
     private connectionservice: ConnectionService,
      private zohoService: ZohoService,private quickbookService: QuickbookService, private alertify: AlertifyService) { }

  ngOnInit() {
    this.getImportedSummaryList();
    this.showImportButton = this.zohoService.isInvoiceLoaded;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getImportedSummaryList() {
    if (this.connectionservice.mappingId !== undefined) {
      this.displaySection = 'loader';
      this.quickbookService.getImportedSummary(this.connectionservice.mappingId)
      .subscribe((data: QboInoviceSummary) => {
        this.displaySection = 'data';
        this.gridData = data;
        console.log(data);
      }, error => {
        this.dialogRef.close();
        console.log(error);
        this.alertify.error('something went wrong.');
      });
    }
  }

  undoImportedRecord(id) {
    this.quickbookService.undoImportedRecord(id)
      .subscribe((data: boolean) => {
       if (data === true) {
         this.gridData.find(
          x => x.id === id
        ).isUndo = true;
       }
      }, error => {
        this.dialogRef.close();
        console.log(error);
        this.alertify.error('something went wrong.');
      });
    }

  getDisplaySection(section) {
    return this.displaySection === section;
  }

  
}
