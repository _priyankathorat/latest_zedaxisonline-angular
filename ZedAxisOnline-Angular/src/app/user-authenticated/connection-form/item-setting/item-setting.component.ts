import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { QbAccountSettingDropdown, QbAccountSettingSubmit } from 'src/app/_models/Interfaces/qb-account-setting-dropdown';
import { MatDialogRef } from '@angular/material';
import { QuickbookService } from 'src/app/_service/quickbook.service';
import { ConnectionService } from 'src/app/_services/connection.service';

@Component({
  selector: 'app-item-setting',
  templateUrl: './item-setting.component.html',
  styleUrls: ['./item-setting.component.css']
})
export class ItemSettingComponent implements OnInit {
  showLoader = false;
  settingForm: FormGroup;
  submitValues: QbAccountSettingSubmit = {};
  dropdownValues: QbAccountSettingDropdown;
  valueList: any[];

  constructor(public dialogRef: MatDialogRef<ItemSettingComponent>,
    private formBuilder: FormBuilder,
    private quickbookService: QuickbookService,
    private connectionservice: ConnectionService) { }

  get type() {
    return this.settingForm.get('type');
  }

  get taxCode() {
    return this.settingForm.get('taxCode');
  }

  get incomeAccount() {
    return this.settingForm.get('incomeAccount');
  }

  get cOGSAccount() {
    return this.settingForm.get('cOGSAccount');
  }

  get assetAccount() {
    return this.settingForm.get('assetAccount');
  }

  ngOnInit() {
    this.initializeForm();
    this.showLoader = true;
    this.quickbookService.getQuickbookItemSettingsDropdownValues()
      .subscribe(response => {
        console.log(response);
        this.showLoader = false;
        this.dropdownValues = response;
        console.log(this.dropdownValues);

        if (this.connectionservice.itemSettingType) {
         this.settingForm.controls.type.setValue(this.connectionservice.itemSettingType);
        }
        if (this.connectionservice.itemSettingTaxCodeId) {
          this.settingForm.controls.taxCode.setValue(this.connectionservice.itemSettingTaxCodeId);
         }
         if (this.connectionservice.itemSettingIncomeId) {
          this.settingForm.controls.incomeAccount.setValue(this.connectionservice.itemSettingIncomeId);
         }
         if (this.connectionservice.itemSettingCogsId) {
          this.settingForm.controls.cOGSAccount.setValue(this.connectionservice.itemSettingCogsId);
         }
         if (this.connectionservice.itemSettingAssetId) {
          this.settingForm.controls.assetAccount.setValue(this.connectionservice.itemSettingAssetId);
         }

        this.setOptionalValidations();
      }, error => console.log(error));
  }

  initializeForm() {
    this.settingForm = this.formBuilder.group({
      type: [undefined, Validators.required],
      taxCode: ['', Validators.required],
      incomeAccount: ['', Validators.required],
      cOGSAccount: ['', Validators.required],
      assetAccount: ['', Validators.required]
    });
  }

  onChanges() {
    this.settingForm.get('type').valueChanges
      .subscribe(selectedType => {
        if (selectedType === this.dropdownValues.itemType[2]) {
          this.removeValidation('assetAccount', true);
          this.addValidation('cOGSAccount');
        } else if (selectedType === this.dropdownValues.itemType[0]) {
          this.removeValidation('assetAccount', true);
          this.removeValidation('cOGSAccount', true);
        } else if (selectedType === this.dropdownValues.itemType[1]) {
          this.addValidation('taxCode');
          this.addValidation('incomeAccount');
          this.addValidation('cOGSAccount');
          this.addValidation('assetAccount');
        }
        this.setOptionalValidations();
      });
  }

  setOptionalValidations() {
    if (this.dropdownValues.taxEntities.length === 0) {
      this.removeValidation('taxCode', false);
    }
    if (this.dropdownValues.incomeAccountEntities.length === 0) {
      this.removeValidation('incomeAccount', false);
    }
    if (this.dropdownValues.cogsAccountEntities.length === 0) {
      this.removeValidation('cOGSAccount', false);
    }
    if (this.dropdownValues.assetAccountEntities.length === 0) {
      this.removeValidation('assetAccount', false);
    }
  }

  onSubmit() {
    if (this.settingForm.valid === true) {
      this.setSubmittedValues();
      this.quickbookService.saveSetting(this.submitValues).subscribe(
        result => {
          console.log(result);
        this.onNoClick();
      });
    }
  }

  onNoClick() {
    this.dialogRef.close();
  }

  removeValidation(control: string, makeDisable: boolean) {
    this.settingForm.get(control).reset();
    this.settingForm.get(control).clearValidators();
    this.settingForm.get(control).updateValueAndValidity();
    if (makeDisable === true) {
      this.settingForm.get(control).disable();
    }
  }

  addValidation(control: string) {
    this.settingForm.get(control).setValidators(Validators.required);
    this.settingForm.get(control).updateValueAndValidity();
    this.settingForm.get(control).enable();
  }
  isControlValid(control: string): boolean {
    if (this.settingForm.get(control).value) {
      return true;
    }
    return false;
  }

  setSubmittedValues () {
    this.submitValues.type = this.settingForm.get('type').value;
    this.connectionservice.itemSettingType = this.settingForm.get('type').value;

      if (this.isControlValid('taxCode') === true) {
        this.submitValues.taxCodeId = this.settingForm.controls.taxCode.value;
        this.connectionservice.itemSettingTaxCodeId = this.settingForm.controls.taxCode.value;
        this.valueList = this.dropdownValues.taxEntities.filter(s => s.id === this.submitValues.taxCodeId);
        this.submitValues.taxCode = this.valueList[0].name;
      }

      if (this.isControlValid('incomeAccount') === true) {
        this.submitValues.incomeAccountId = this.settingForm.controls.incomeAccount.value;
        this.connectionservice.itemSettingIncomeId = this.settingForm.controls.incomeAccount.value;
        this.valueList = this.dropdownValues.incomeAccountEntities.filter(s => s.id === this.submitValues.incomeAccountId);
        this.submitValues.incomeAccount = this.valueList[0].name;
      }

      if (this.isControlValid('cOGSAccount') === true) {
        this.submitValues.cOGSAccountId = this.settingForm.controls.cOGSAccount.value;
        this.connectionservice.itemSettingCogsId = this.settingForm.controls.cOGSAccount.value;
        this.valueList = this.dropdownValues.cogsAccountEntities.filter(s => s.id === this.submitValues.cOGSAccountId);
        this.submitValues.cOGSAccount = this.valueList[0].name;
      }

      if (this.isControlValid('assetAccount') === true) {
        this.submitValues.assetAccountId = this.settingForm.controls.assetAccount.value;
        this.connectionservice.itemSettingAssetId = this.settingForm.controls.assetAccount.value;
        this.valueList = this.dropdownValues.assetAccountEntities.filter(s => s.id === this.submitValues.assetAccountId);
        this.submitValues.assetAccount = this.valueList[0].name;
      }
  }
}
