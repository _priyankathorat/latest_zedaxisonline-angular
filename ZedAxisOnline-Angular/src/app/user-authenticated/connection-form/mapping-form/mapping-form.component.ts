import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MappedField, ConstantField } from 'src/app/_models/Interfaces/MappedFields';
import { ConnectionService } from 'src/app/_services/connection.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { EditFunctionPopupComponent } from './edit-function-popup/edit-function-popup.component';
import { CreateFunctionPopupComponent } from './create-function-popup/create-function-popup.component';
import { ZohoService } from 'src/app/_service/zoho.service';
import { MappingFunctionService } from 'src/app/_service/mapping-function.service';
import { MappingService } from 'src/app/_service/mapping.service';
import { ItemSettingComponent } from '../item-setting/item-setting.component';

@Component({
  selector: 'app-mapping-form',
  templateUrl: './mapping-form.component.html',
  styleUrls: ['./mapping-form.component.css']
})
export class MappingFormComponent implements OnInit {
  @ViewChild('dropdownlist') public dropdownlist: any;
  @Output() GotoStepper = new EventEmitter<number>();

  model: any;
  SelectedFilterButtonId: number;
  transCol: any[];
  ConnectionTypesId: number;
  connectCol: any[];
  ConnectionColumnsData: any[];
  MappedFields: MappedField[] = [];
  MappedFieldEntry: MappedField = {};
  ConstantFields: ConstantField[];
  dropdownTransValue: String;
  dropdownConValue: String;
  disableSaveMapping = true;
  disableInvoiceListButton = true;
  selectFieldtext = 'Select field to map';
  selectFunctiontext = 'Select function';
  public defaultItem: { name: string; id: number } = {
    name: 'Select function',
    id: 0
  };
  public functionList: any[] = [];
  public updateFunctionList: any[] = [
    { id: -1, name: 'Create Function' },
    { id: -2, name: 'Edit Function' }
  ];

  filterData: any[];
  gridData: any[];
  mappingModel: any = {};

  receivePaymentList: any[] = [
    'Account Name',
    'Account ID',
    'Description',
    'Account Owner ID'
  ];

  constructor(
    private connectionservice: ConnectionService,
    public dialog: MatDialog,
    private zohoService: ZohoService,
    private alertifyService: AlertifyService,
    private functionService: MappingFunctionService,
    private mappingService: MappingService
  ) {
    if (this.connectionservice.gridData != null) {
      this.gridData = this.connectionservice.gridData;
      this.filterData = this.connectionservice.gridData;
    }
    this.dropdownTransValue = this.connectionservice.transSelectedValue;
    this.ConnectionTypesId = this.connectionservice.ConnectionTypesId;

    if (this.connectionservice.conColList != null) {
      this.connectCol = this.connectionservice.conColList.map(a => a.name);
      this.ConnectionColumnsData = this.connectionservice.conColList;
      this.dropdownConValue = this.connectionservice.conSelectedValue;
    }
  }
  ngOnInit() {
    this.ConnectionTypesId = this.connectionservice.ConnectionTypesId;
    console.log('this.ConnectionTypesId');
    console.log(this.ConnectionTypesId);

    this.GetFunctionList();
  }
  public onFunctionDropdownChange(value: any, dataID: number): void {
    if (value !== 0 && value === -2) {
      const dialogRef = this.dialog.open(EditFunctionPopupComponent, {
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        this.GetFunctionList();
        const record = this.filterData.filter(x => x.id === dataID);

        record[0].selectedFunction = 0;
        // this.dropdownlist.selectedItem(0);
      });
    } else if (value === -1) {
      const dialogRef = this.dialog.open(CreateFunctionPopupComponent, {
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        this.GetFunctionList();
        const record = this.filterData.filter(x => x.id === dataID);

        record[0].selectedFunction = 0;
      });
    } else {
      const record = this.filterData.filter(x => x.id === dataID);

      if (value !== '') {
        record[0].selectedFunctionId = value;
      }
    }
  }

  public getAllFields(id: number): void {
    this.gridData = this.filterData;
    this.addClass(id);
    //  this.unMappedGridData = acountDetail;
    //  this.mySelection = [];
  }

  public getMappedFields(id: number): void {
    this.gridData = this.filterData.filter(x => x.isMapped === 1);
    this.addClass(id);

  }

  public getUnMappedFields(id: number): void {
    this.gridData = this.filterData.filter(x => x.isMapped === 0);
    this.addClass(id);

  }

  addClass(id: number) {
    this.SelectedFilterButtonId = id;
  }

  public valueChange(value: any, dataID: any): void {
    const record = this.filterData.filter(x => x.id === dataID);
    if (value !== this.selectFieldtext) {
      record[0].isMapped = 1;
      record[0].constantValue = '';
    } else {
      record[0].isMapped = 0;
    }

    const mappedRows = this.filterData.filter(x => x.isMapped === 1);
    if (mappedRows.length === 0) {
      this.disableSaveMapping = true;
    } else {
      this.disableSaveMapping = false;
    }
  }

  public constantValueChange(value: any, dataID: any): void {
    const record = this.filterData.filter(x => x.id === dataID);

    if (value.trim() !== '') {
      record[0].selectedItem = this.selectFieldtext;
      record[0].isMapped = 1;
    } else {
      record[0].isMapped = 0;
    }
    const mappedRows = this.filterData.filter(x => x.isMapped === 1);
    if (mappedRows.length === 0) {
      this.disableSaveMapping = true;
    } else {
      this.disableSaveMapping = false;
    }
  }

  public GetFunctionList(): void {
    console.log('called');
    this.functionService.getFunctions().subscribe(
      (data: any) => {
        console.log(data);
      this.functionList = [];
      this.functionList = [
        { id: -1, name: 'Create Function' },
        { id: -2, name: 'Edit Function' }
      ];
      data.forEach(item => {
        this.functionList.push(item);
      });
    }, error => (console.log(error)));
    // this.connectionservice.GetFunctionList().subscribe(data => {
    //   this.functionList = [];
    //   this.functionList = [
    //     { id: 1, name: 'Create Function' },
    //     { id: 2, name: 'Edit Function' }
    //   ];
    //   data.forEach(item => {
    //     this.functionList.push(item);
    //   });
    // });
  }

  SaveMapping() {
    this.disableSaveMapping = true;

    const mappedRows = this.filterData.filter(x => x.isMapped === 1);

    if (!this.connectionservice.MappingName) {
      this.GotoStepper.emit(0);
      this.disableSaveMapping = false;

      this.alertifyService.error('Please enter mapping name');
      return;
    }
    if (!this.connectionservice.ValidMappingName) {
      this.GotoStepper.emit(0);
      this.disableSaveMapping = false;

      this.alertifyService.error('Please enter valid mapping name');
      return;
    }
    if (mappedRows.length === 0) {
      this.disableSaveMapping = false;

      this.alertifyService.dialog('Please map fields');
      return;
    }

    const selectedItem = mappedRows.map(x => x.selectedItem);
    this.MappedFields = [];
    mappedRows.forEach(Item => {
      this.MappedFieldEntry = {};
      console.log(this.ConnectionColumnsData);
      if (Item.constantValue === '') {
      const connTransId = this.ConnectionColumnsData.find(
        x => x.name === Item.selectedItem
      ).id;
      this.MappedFieldEntry.ConnectionTransColumnId = connTransId;
      }
      this.MappedFieldEntry.qboTransColumnId = Item.id;
      this.MappedFieldEntry.constantValue = Item.constantValue;
      this.MappedFieldEntry.selectedFunctionId = Item.selectedFunctionId;
      this.MappedFields.push(this.MappedFieldEntry);
    });
    this.mappingModel.Name = this.connectionservice.MappingName;
    this.mappingModel.MappedField = this.MappedFields;
    this.mappingModel.connectionTypeId = this.connectionservice.connectionTypeId;
    this.mappingModel.ConnectionTransactionTypeId = this.connectionservice.connectionTransTypeId;
    this.mappingModel.QuickbookTransactionTypeId = this.connectionservice.QBOTransTypeId;

    this.mappingService.saveMapping(this.mappingModel).subscribe((data: any) => {
      this.connectionservice.mappingId = data.mappingId;
      this.disableSaveMapping = true;
      this.disableInvoiceListButton = false;
      this.alertifyService.success('mapping saved successfully');
    }, error => (console.log(error)));
  }

  getInvoiceList() {
    if (this.connectionservice.mappingId !== undefined && !this.zohoService.isInvoiceLoaded) {
      this.zohoService.callInvoiceList.emit();
      this.GotoStepper.emit(3);
    } else {
      if (!this.disableInvoiceListButton) {
        this.GotoStepper.emit(3);
      }
    }
  }

  quickbookItemSetting(): void {
    const dialogRef = this.dialog.open(ItemSettingComponent, {
      width: '650px',
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }
}
