import { Component, OnInit, Inject } from '@angular/core';
import { State } from '@progress/kendo-data-query';
import { IdName } from 'src/app/_models/Interfaces/IdName';
import { EditService } from '../edit.service';
import { MappingFunction } from '../model';
import { FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
// import { DialogEditFunctionComponent } from '../mapping-form.component';
import { ConnectionService } from 'src/app/_services/connection.service';
import { MappingFunctionService } from 'src/app/_service/mapping-function.service';

@Component({
  selector: 'app-edit-function-popup',
  templateUrl: './edit-function-popup.component.html',
  styleUrls: ['./edit-function-popup.component.css']
})
export class EditFunctionPopupComponent implements OnInit {

  public view: any;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };
  EditFunctionList: Array<IdName> = [];
  functionId: number;
  btnAddNewRow: boolean;
  editGridData = [];
  selectedFunction = 0;
  private editService: EditService;
  private editedRowIndex: number;
  private editedProduct: MappingFunction;
  public defaultItem: { name: string; id: number } = {
    name: 'Select function',
    id: 0
  };
  public functionList: Array<IdName> = [
    { name: 'Create Function', id: 1 },
    { name: 'Edit Function', id: 2 }
  ];

  public formGroup: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<EditFunctionPopupComponent>,
    @Inject(EditService) editServiceFactory: any,
    private connectionservice: ConnectionService,
    private functionService: MappingFunctionService
  ) {
    this.editService = editServiceFactory();
  }

  public ngOnInit(): void {
    this.GetFunctionList();
    // this.view = this.editService.pipe(map(data => process(data, this.gridState)));
    // this.editService.read();
    this.view = this.editGridData;
  }
  // public onStateChange(state: State) {
  //   this.gridState = state;

  //   this.editService.read();
  // }

  public addHandler({ sender }, formInstance) {
    console.log('this is called');
    // formInstance.reset();
    this.closeEditor(sender);
    sender.addRow(new MappingFunction());
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);

    this.editedRowIndex = rowIndex;
    this.editedProduct = Object.assign({}, dataItem);

    sender.editRow(rowIndex);
    console.log(this.editedProduct);
  }

  public cancelHandler({ sender, rowIndex }) {
    const data: any = { ...this.editedProduct};
    const originalDataItem = this.editGridData.find(item => item.id === data.id);
    // revert changes
    if (originalDataItem) {
        Object.assign(originalDataItem, data);
    }
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({ sender, rowIndex, dataItem, isNew }) {
    sender.closeRow(rowIndex);
    if (isNew) {
      dataItem.functionId = this.functionId;
    }
    this.functionService.updateFunction(dataItem).subscribe((data: any) => {
      dataItem.id = data.id;
      if (isNew) {
        if (this.editGridData === undefined) {
          this.editGridData = dataItem;
        } else {
          this.editGridData.push(dataItem);
        }
        this.view = this.editGridData;
      }
    });
    this.editedRowIndex = undefined;
    this.editedProduct = undefined;
  }

  public removeHandler({ dataItem }) {
    this.connectionservice.DeleteFunctionData(dataItem).subscribe(() => {
      this.editGridData = this.editGridData.filter(a => a.id !== dataItem.id);
      this.view = this.editGridData;
    });
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    // this.editService.resetItem(this.editedProduct);
    this.editedRowIndex = undefined;
    this.editedProduct = undefined;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  public GetFunctionList(): void {
    this.functionService.getFunctions().subscribe((data: []) => {
      this.EditFunctionList = data;
    });
  }

  public onEditFunctionChange(value: any): void {
    if (value !== 0) {
      this.functionService.getFunction(value).subscribe((data: [])=> {
        if (data.length !== 0) {
          this.editGridData = data;
          this.view = this.editGridData;
        }
        this.functionId = value;
        this.btnAddNewRow = true;
      });
    } else {
      this.btnAddNewRow = false;
    }
  }

}
