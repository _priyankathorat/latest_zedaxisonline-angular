import { Component, OnInit, Inject } from '@angular/core';
import { State } from '@progress/kendo-data-query';
import { FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
// import { DialogEditFunctionComponent } from '../mapping-form.component';
import { EditService } from '../edit.service';
import { MappingFunction } from '../model';
import { ConnectionService } from 'src/app/_services/connection.service';
import { EditFunctionPopupComponent } from '../edit-function-popup/edit-function-popup.component';
import { MappingFunctionService } from 'src/app/_service/mapping-function.service';

@Component({
  selector: 'app-create-function-popup',
  templateUrl: './create-function-popup.component.html',
  styleUrls: ['./create-function-popup.component.css']
})
export class CreateFunctionPopupComponent implements OnInit {

  public view: any;

  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };
  private editService: EditService;
  private editedRowIndex: number;
  private AddMapping: MappingFunction;
  public formGroup: FormGroup;
  IsNameEntered: boolean;
  public functionId = 0;
  editGridData: any[];
  txtFunctionName: string;
  errorFunctionName: string;
  constructor(
    public dialogRef: MatDialogRef<EditFunctionPopupComponent>, // may be here is CreateFunctionPopupComponent
    @Inject(EditService) editServiceFactory: any,
    private connectionservice: ConnectionService,
    private functionService: MappingFunctionService
  ) {
    this.editService = editServiceFactory();
  }
  public ngOnInit(): void {}

  public addHandler({ sender }, formInstance) {
    // formInstance.reset();
    this.closeEditor(sender);
    sender.addRow(new MappingFunction());
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);

    this.editedRowIndex = rowIndex;

    sender.editRow(rowIndex);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({ sender, rowIndex, dataItem, isNew }) {
    if (this.txtFunctionName === '') {
      // this.errorFunctionName
      return;
    }
    sender.closeRow(rowIndex);
    if (this.functionId === 0) {
      dataItem.Name = this.txtFunctionName;
      this.functionService.saveFunction(dataItem).subscribe(data => {
        console.log(data);
        dataItem = data;
        this.editGridData = dataItem;
        this.view = this.editGridData;
        const record = this.editGridData.filter(x => x.functionId !== 0);
        this.functionId = record[0].functionId;
        console.log(this.functionId);
      });
    } else {
      dataItem.functionId = this.functionId;
      dataItem.Name = this.txtFunctionName;
      this.functionService.updateFunction(dataItem).subscribe((data: any) => {
        console.log(data);
        dataItem.id = data.id;
        if (isNew) {
          this.editGridData.push(dataItem);
          this.view = this.editGridData;
        }
        console.log(this.view);
      });
    }
    this.editedRowIndex = undefined;
    this.AddMapping = undefined;
  }

  public removeHandler({ dataItem }) {
    this.connectionservice.DeleteFunctionData(dataItem).subscribe(() => {
      this.editGridData = this.editGridData.filter(a => a.id !== dataItem.id);
      this.view = this.editGridData;
    });
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editService.resetItem(this.AddMapping);
    this.editedRowIndex = undefined;
    this.AddMapping = undefined;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  FunctionName(dataItem) {
    if (this.txtFunctionName !== '') {
      this.IsNameEntered = true;
    }
  }

}
