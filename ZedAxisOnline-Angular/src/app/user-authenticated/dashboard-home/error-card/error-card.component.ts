import { Component, OnInit } from '@angular/core';
import { trigger, style, state, transition, animate  } from '@angular/animations';


@Component({
  selector: 'app-error-card',
  templateUrl: './error-card.component.html',
  styleUrls: ['./error-card.component.css'],
  animations: [
    trigger('frontView', [
      state('normal', style({
        transform: 'rotateY(0deg)',
         opacity: 1,
         'visibility': 'visible',
         'backface-visibility': 'hidden'
      })),
      state('highlighted', style({
        transform: 'rotateY(180deg)',
        opacity: 0,
        'visibility': 'hidden'
      })),
      // transition('normal => highlighted', animate(400))
    ]),

    trigger('backView', [
      state('normal', style({
        transform: 'rotateY(180deg)',
        opacity: 0,
        'visibility': 'hidden',
        'backface-visibility': 'hidden'
    })),
      state('highlighted', style({
        transform: 'rotateY(0deg)',
        opacity: 1,
        'visibility': 'visible'
      })),
      // transition('normal <=> highlighted', animate(400))
    ])
  ]
})
export class ErrorCardComponent implements OnInit {
  state = 'normal';
 
  constructor() { }

  ngOnInit() {
  }

  onAnimate() {
    this.state === 'normal' ? this.state = 'highlighted' : this.state = 'normal';
  }

  reverseAnimate() {
    this.state === 'normal' ? this.state = 'highlighted' : this.state = 'normal';
  }
}
