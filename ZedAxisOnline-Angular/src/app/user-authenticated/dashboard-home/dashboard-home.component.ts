import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.css'],
})
export class DashboardHomeComponent implements OnInit {
  @Input() showEditProfile: boolean;
  @Input() showConnection: boolean;
  @Input() showDashboard: boolean;

  constructor() { }

  ngOnInit() {
  }

}
