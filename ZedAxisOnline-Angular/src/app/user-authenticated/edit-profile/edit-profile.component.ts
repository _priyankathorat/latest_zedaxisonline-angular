import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { UserInfo } from 'src/app/_models/Interfaces/UserInfo';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { UserService } from 'src/app/_service/user.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  userInfo: UserInfo;
  InvalidFile = false;
  selectedFile: File;

  constructor(
    private userService: UserService,
    private alertifyService: AlertifyService,
    private router: Router
  ) {}

  ngOnInit() {
    this.userInfo = this.userService.getUserProfileInfo();
  }

  onFileSelect(event) {
    if (event.target.files && event.target.files[0]) {
      this.selectedFile = <File>event.target.files[0];
      const fileExt = this.selectedFile.name.toLowerCase().split('.').pop();
      if (fileExt === 'jpg' || fileExt === 'jpeg' || fileExt === 'png') {
        // read file and assign to img tag
        const reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = eventComplete => this.userInfo.photoUrl = eventComplete.target['result'];
        this.InvalidFile = false;
      } else {
        this.InvalidFile = true;
        return;
      }
    }
  }

  editProfile() {
    const uploadData = new FormData();
    if (!this.InvalidFile && this.selectedFile) {
      uploadData.append('photoUpload', this.selectedFile, this.selectedFile.name);
    }
    uploadData.set('firstName', this.userInfo.firstName);
    uploadData.set('lastName', this.userInfo.lastName);
    uploadData.set('role', this.userInfo.role.toString());
    this.userService.updateUserProfileAtServer(uploadData)
    .subscribe((user: UserInfo ) => {
       this.userService.updateUserProfileInfo(user);
       this.alertifyService.success('User Updated succesfully');
      }
    );
  }

  cancelClick() {
    this.router.navigate(['/dashboard']);
  }
}
