import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { ConnectionService } from 'src/app/_services/connection.service';
import { QuickbookService } from 'src/app/_service/quickbook.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connect-to-quickbook',
  templateUrl: './connect-to-quickbook.component.html',
  styleUrls: ['./connect-to-quickbook.component.css']
})
export class ConnectToQuickbookComponent implements OnInit {

  showLoader = true;
  disConnectLoader = true;
  isConnected: boolean;
  isDisconnected: boolean;
  companyName = '';

  constructor(private authService: AuthService, private quickbookService: QuickbookService, 
    private alertify: AlertifyService, private router: Router) { }

  ngOnInit() {
      this.quickbookService.getQuickbookConnectionDetails().subscribe(
        (response: any) => {
          this.showLoader = false;
          this.disConnectLoader = false;
          if (response) {
            this.isConnected = true;
            this.isDisconnected = true;
            this.companyName = response.companyName;
          } else {
            this.isConnected = false;
            this.isDisconnected = false;
          }
        }
      );
  }

  quickbookLogin() {
    this.showLoader = true;
    this.quickbookService.getQuickbookConnectUrl()
    .subscribe(
      url => (window.location.href = url),
      () => (this.showLoader = false));
  }

  quickBookDisconnect() {
    this.disConnectLoader = true;
    this.quickbookService.disconnectFromQuickBook()
    .subscribe(result => {
      console.log(result);
      if (result) {
        this.router.navigate(['/connect-to-quickbook']);
        this.alertify.success('Disconnected from Quickbook');
        this.isConnected = false;
        this.isDisconnected = false;
      } else {
        this.alertify.error('Something went wrong.');
        this.isConnected = true;
        this.isDisconnected = true;
    }
    this.disConnectLoader = false;
    });
  }
}
