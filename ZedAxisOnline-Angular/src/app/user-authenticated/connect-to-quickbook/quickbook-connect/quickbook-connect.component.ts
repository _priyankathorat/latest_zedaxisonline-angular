import { Component, OnInit } from '@angular/core';
import { QuickbookService } from 'src/app/_service/quickbook.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from 'src/app/_services/alertify.service';

@Component({
  selector: 'app-quickbook-connect',
  templateUrl: './quickbook-connect.component.html',
  styleUrls: ['./quickbook-connect.component.css']
})
export class QuickbookConnectComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private quickbookService: QuickbookService,
    private router: Router, private alertify: AlertifyService
  ) {}

  ngOnInit() {
    if (this.route.snapshot.queryParams && this.route.snapshot.queryParams['code']) {
      this.quickbookService.connectToQuickbook(this.route.snapshot.queryParams)
      .subscribe(result => {
        console.log(result);
        if (result) {
          this.router.navigate(['/connect-to-quickbook']);
          this.alertify.success('Connected to Quickbook');
        } else {
          this.router.navigate(['/dashboard']);
          this.alertify.error('Something went wrong.');
      }
      }, errorResponse => {
        this.router.navigate(['/dashboard']);
        this.alertify.error('Something went wrong.');
      });
    } else {
      this.router.navigate(['/dashboard']);
      this.alertify.error('Not able to connect to intuit.');
    }
  }

}
