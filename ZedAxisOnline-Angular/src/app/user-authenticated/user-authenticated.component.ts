import { Component, OnInit } from '@angular/core';

import { UserInfo } from '../_models/Interfaces/UserInfo';
import { UserService } from '../_service/user.service';
import { ConnectionService } from '../_services/connection.service';

@Component({
  selector: 'app-user-authenticated',
  templateUrl: './user-authenticated.component.html',
  styleUrls: ['./user-authenticated.component.css']
})
export class UserAuthenticatedComponent implements OnInit {
  active = false;
  userInfo: UserInfo;

  constructor(private userService: UserService,
    private connectionservice: ConnectionService) { }

  ngOnInit() {
    this.userInfo = this.userService.getUserProfileInfo();
    this.userService.UserUpdatedEvent.subscribe(user => this.userInfo = user);
    this.disableItemsettings();
  }

  disableItemsettings() {
    this.connectionservice.itemSettingType = null;
    this.connectionservice.itemSettingTaxCodeId = null;
    this.connectionservice.itemSettingIncomeId = null;
    this.connectionservice.itemSettingCogsId = null;
    this.connectionservice.itemSettingAssetId = null;
  }
}
