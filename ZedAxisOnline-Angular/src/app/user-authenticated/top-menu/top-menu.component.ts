import { Component, OnInit } from '@angular/core';
import { UserInfo } from 'src/app/_models/Interfaces/UserInfo';
import { AuthService } from 'src/app/_service/auth.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_service/user.service';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.css']
})
export class TopMenuComponent implements OnInit {
  userInfo: UserInfo;

  constructor(private authService: AuthService, private router: Router, private userService: UserService) {}

  ngOnInit() {
    this.userInfo = this.userService.getUserProfileInfo();
    this.userService.UserUpdatedEvent.subscribe(user => this.userInfo = user);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
