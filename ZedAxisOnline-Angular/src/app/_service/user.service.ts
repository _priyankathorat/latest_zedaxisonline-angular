import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserInfo } from '../_models/Interfaces/UserInfo';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private apiBaseUrl = environment.ApiUrl + 'user/';
  private loggedInUser: UserInfo;
  UserUpdatedEvent = new EventEmitter<UserInfo>();

  constructor(private http: HttpClient) {}

  getUserProfileFromServer() {
    return this.http.get<string>(this.apiBaseUrl + 'profile');
  }

  updateUserProfileAtServer(data: FormData) {
    console.log(data);
    return this.http.put(this.apiBaseUrl + 'profile', data);
  }

  getUserProfileInfo() {
      this.loggedInUser = JSON.parse(localStorage.getItem('user'));
    return {...this.loggedInUser};
  }

  updateUserProfileInfo(user: UserInfo) {
    localStorage.setItem('user', JSON.stringify(user));
    this.loggedInUser = JSON.parse(localStorage.getItem('user'));
    this.UserUpdatedEvent.emit({...this.loggedInUser});
  }
}
