import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AllConnectionsResponse } from '../_models/Interfaces/AmazonResponse';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
  private apiBaseUrl = environment.ApiUrl + 'general/';

  constructor(private http: HttpClient) {}

  getAllConnections() {
    return this.http.get( this.apiBaseUrl + 'connections');
  }
}
