import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MappingService {
  private apiBaseUrl = environment.ApiUrl + 'mapping/';

  constructor(private http: HttpClient) {}

  getConnectionTransactionTypes(id) {
    return this.http.get(this.apiBaseUrl + 'connection/' + id + '/TransactionTypes');
  }

  getQuickbookTransactionTypes() {
    return this.http.get(this.apiBaseUrl + 'quickbook/TransactionTypes');
  }

  getConnectionTransactionColumn(id) {
    return this.http.get(this.apiBaseUrl + 'connection/' + id + '/transactioncolumns');
  }

  getQuickbookTransactionColumn(id) {
    return this.http.get(this.apiBaseUrl + '');
  }


  getDataForMappping(model: any) {
    console.log(model);
    return this.http.get(this.apiBaseUrl + 'CreateMapping?qboTransationId=' + model.QboTransId);
  }

  validateMappingName(mappingName: string) {
    return this.http.get(this.apiBaseUrl + 'ValidateMappingName?mappingName=' + mappingName);
  }

  saveMapping(model: any) {
    console.log(model);
    return this.http.post(this.apiBaseUrl + 'SaveMappingData', model);
  }
}
