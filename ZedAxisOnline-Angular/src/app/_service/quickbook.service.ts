import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { QboInoviceSummary } from '../_models/Interfaces/qbo-inovice-summary';
import { QbAccountSettingDropdown } from '../_models/Interfaces/qb-account-setting-dropdown';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuickbookService {
  private apiBaseUrl = environment.ApiUrl + 'quickbook/';

  constructor(private http: HttpClient) {}

  getQuickbookConnectUrl () {
    return this.http.get<string>(this.apiBaseUrl + 'authorizationUrl')
      .map((response: any) => (response.authorizationUrl));
  }

  getQuickbookConnectionDetails () {
    return this.http.get<string>(this.apiBaseUrl + 'connection/companyname');
  }

  connectToQuickbook(model: any) {
    return this.http.post(this.apiBaseUrl +  'accessToken', model)
    .map((response: any) => {
      if (response) {
        return true;
      } else {
        return false;
      }
    });
  }

  undoImportedRecord(mappingId) {
    return this.http.get(this.apiBaseUrl + 'undo?importSummaryId=' + mappingId);
  }

  getImportedSummary(mappingId: number) {
    return this.http.get<QboInoviceSummary>(
      this.apiBaseUrl + 'Import?' + 'mappingId=' + mappingId
    );
  }

  getQuickbookItemSettingsDropdownValues(): Observable<QbAccountSettingDropdown> {
    return this.http.get<QbAccountSettingDropdown>(this.apiBaseUrl + 'itemsetting');
  }

  saveSetting(values) {
    return this.http.post(this.apiBaseUrl + 'SaveItemSetting', values);
  }

  disconnectFromQuickBook() {
    return this.http.get(this.apiBaseUrl +  'DisconnectFromQuickBook')
    .map((response: any) => {
      if (response) {
        return true;
      } else {
        return false;
      }
    });
  }
}
