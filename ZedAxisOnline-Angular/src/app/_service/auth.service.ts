import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import 'rxjs/add/operator/map';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiBaseUrl = environment.ApiUrl + 'auth/';
  private jwthelper = new JwtHelperService();

  constructor(private http: HttpClient) {}

  login(model: any) {
    return this.http
      .post(this.apiBaseUrl + 'signin', model)
      .map((response: any) => this.checkTokenAndSave(response));
  }

  register(model: any) {
    return this.http
      .post(this.apiBaseUrl + 'signup', model)
      .map((response: any) => this.checkTokenAndSave(response));
  }

  getIntuitSignInUrl () {
    return this.http.get<string>(this.apiBaseUrl + 'Intuit/SignInUrl')
      .map((response: any) => (response.authorizationUrl));
  }

  signInWithIntuit (model: any) {
    return this.http.post(this.apiBaseUrl + 'Intuit/SignIn', model)
    .map((response: any) => (this.checkTokenAndSave(response)));
  }

  logout() {
    localStorage.clear();
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    if (!token) {
      return false;
    }
    return !this.jwthelper.isTokenExpired(token);
  }

  private checkTokenAndSave (response: any): boolean {
    if (response && response.token) {
      localStorage.setItem('token', response.token);
      localStorage.setItem('user', JSON.stringify(response.user));
      return true;
    }
    return false;
  }
}
