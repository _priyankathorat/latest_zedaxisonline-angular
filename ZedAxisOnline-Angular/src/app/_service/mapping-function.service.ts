import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MappingFunctionService {
  private apiBaseUrl = environment.ApiUrl + 'function/';

  constructor(private http: HttpClient) {}

  getFunctions() {
    return this.http.get(this.apiBaseUrl + 'all');
  }

  saveFunction(model: any) {
    return this.http.post(this.apiBaseUrl + 'save', model);
  }

  updateFunction(data: any) {
    console.log(data);
    return this.http.put(this.apiBaseUrl + 'update', data);
  }

  getFunction(id) {
    console.log(id);
    return this.http.get(this.apiBaseUrl + 'get/' + id);
  }
}
