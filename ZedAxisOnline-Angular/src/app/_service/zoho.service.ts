import { Injectable, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
// tslint:disable-next-line: max-line-length
import { ZohoOrganizationDetailResponse, ZohoOrganizationDetails, SaveConnection, ZohoWebHookDetails } from '../_models/Interfaces/ZohoOrganizationDetails';
import { AllConnectionsResponse } from '../_models/Interfaces/AmazonResponse';
import { InvoiceData } from '../_models/Interfaces/invoice-data';
import { QboInoviceSummary } from '../_models/Interfaces/qbo-inovice-summary';

@Injectable({
  providedIn: 'root'
})
export class ZohoService {
  private apiBaseUrl = environment.ApiUrl + 'zoho/';
  isInvoiceLoaded = false;
  selectedConnection: AllConnectionsResponse;
  // invoiceList: InvoiceData;
  callInvoiceList = new EventEmitter<number>();
  constructor(private http: HttpClient) {}

  getOrgnizationDetails(authToken): Observable<ZohoOrganizationDetails> {
    console.log(authToken);
    return this.http.get<ZohoOrganizationDetails>(
      this.apiBaseUrl + 'OrganizationDetails?authToken=' + authToken
    );
  }

  saveConnection(model: SaveConnection) {
    return this.http.post(this.apiBaseUrl + 'connection', model);
  }

  getGuidForWebHook() {
    return this.http.get(this.apiBaseUrl + 'webhookguid');
  }

  saveWebHook(model: ZohoWebHookDetails) {
    return this.http.post(this.apiBaseUrl + 'webhook', model);
  }

  getInvoice(connectionId: number, mappingId: number) {
    return this.http.get<InvoiceData>(
      this.apiBaseUrl + 'invoicelist?' + 'connectionId=' + connectionId  + '&mappingId=' + mappingId
    );
  }

  
}
