import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AmazonService {
  private apiBaseUrl = environment.ApiUrl + 'amazon/';

  constructor(private http: HttpClient) {}

  getAmazonConnectUrl () {
    return this.http.get<string>(this.apiBaseUrl + 'signInUrl')
      .map((response: any) => (response.authorizationUrl));
  }

  validateAccessToken(accessToken: string) {
    return this.http.post(this.apiBaseUrl + 'accessToken', {code: accessToken})
       .map((response: any) => {
        if(response && response.message) {
          return true;
        } else {
          return false;
        }
       });
  }
}
