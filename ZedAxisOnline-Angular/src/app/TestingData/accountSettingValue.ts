import { QbAccountSettingDropdown } from '../_models/Interfaces/qb-account-setting-dropdown';

export const dropdownValue: QbAccountSettingDropdown = {
    itemType: ['Service', 'Inventory', 'Non-Inventory'],
    taxEntities: [{id: '10', name: '10% tax'}, {id: '20', name: '20% tax'}, {id: '30', name: '30% tax'}],
    cogsAccountEntities: [
        {id: '1', name: 'Cost of sales'},
        {id: '2', name: 'Cost of sales 2'},
        {id: '1', name: 'Cost of sales 3'}],
    assetAccountEntities: [
        {id: '1', name: 'Inventory Asset'},
        {id: '2', name: 'Inventory Asset 1'},
        {id: '1', name: 'Inventory Asset 2'}],
    incomeAccountEntities: [
        {id: '1', name: 'sales of product income 1'},
        {id: '2', name: 'sales of product income 2'},
        {id: '1', name: 'sales of product income 3'}]
  };
