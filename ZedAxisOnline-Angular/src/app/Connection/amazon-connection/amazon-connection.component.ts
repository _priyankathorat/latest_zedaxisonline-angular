import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AmazonResponse } from 'src/app/_models/Interfaces/AmazonResponse';
import { ConnectionService } from 'src/app/_services/connection.service';
import { map } from 'rxjs/operators';
import { AmazonService } from 'src/app/_service/amazon.service';
import { AlertifyService } from 'src/app/_services/alertify.service';

@Component({
  selector: 'app-amazon-connection',
  templateUrl: './amazon-connection.component.html',
  styleUrls: ['./amazon-connection.component.css']
})
export class AmazonConnectionComponent implements OnInit {
  amazonresponse: AmazonResponse;

  constructor(private route: ActivatedRoute, private router: Router,
     private connectionservice: ConnectionService, private alertify: AlertifyService,  private amazonService: AmazonService ) { }

  ngOnInit() {
    console.log('fragment');
    console.log(this.route.snapshot);
    const param = new URLSearchParams(this.route.snapshot.fragment);
    const accessToken = param.get('access_token');
    this.amazonService.validateAccessToken(accessToken).subscribe(result => {
      if (result) {
        this.alertify.success('Connection saved sucessfully.');
      } else {
        this.alertify.error('Something went wrong.');
      }
      this.router.navigate(['/connection']);
    });
    // this.route.snapshot.fragment
    // this.route.fragment
    // .pipe(
    //   map(fragment => new URLSearchParams(fragment)),
    //   map(params => ({
    //     AccessToken: params.get('access_token'),
    //   //  error: params.get('error'),
    //   }))
    // )
    // .subscribe(res => this.amazonresponse = res
    // );
this.AmazonGetCode();
}

AmazonGetCode() {
  // console.log(this.amazonresponse);
  // this.amazonresponse.userId = +localStorage.getItem('userId');
  // this.connectionservice.AmazonGetCode(this.amazonresponse).subscribe( () => {
  //   console.log(this.amazonresponse);
  // }, error => {
  //  console.log(error.error);
  // });
}
}
