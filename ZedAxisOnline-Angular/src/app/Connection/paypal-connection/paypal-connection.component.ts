import { ConnectionService } from './../../_services/connection.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-paypal-connection',
  templateUrl: './paypal-connection.component.html',
  styleUrls: ['./paypal-connection.component.css']
})
export class PaypalConnectionComponent implements OnInit {
  code: string;
 error: string;
  state: string;
  realmId: string;
  model: any = {};
  constructor(private route: ActivatedRoute, private router: Router, private connectionService: ConnectionService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      if (this.route.snapshot.queryParams['error']) {
        // this.error = params['error'];
        // if (this.error) {
          this.router.navigate(['/user/dashboard']);
        // }
      }
      if (this.route.snapshot.queryParams['code']) {
        this.model.code = params['code'];
        this.model.UserId = localStorage.getItem('userId');
        this.connectionService.PayPalLogin(this.model)
        .subscribe(data => {
          console.log(window);
         
          if (window != top) {
            top.location.replace('/user/dashboard');
            console.log('from inside if of top')
          }
          window.close();
          console.log('from out of top')
        });
        console.log(this.code);
      }
  });
}


}
