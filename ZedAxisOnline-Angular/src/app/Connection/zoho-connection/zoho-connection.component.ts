import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ZohoService } from 'src/app/_services/zoho.service';
import { Auth } from 'src/app/_models/Interfaces/auth';

@Component({
  selector: 'app-zoho-connection',
  templateUrl: './zoho-connection.component.html',
  styleUrls: ['./zoho-connection.component.css']
})
export class ZohoConnectionComponent implements OnInit {

  constructor(private route: ActivatedRoute, private zohoService: ZohoService, private router: Router) { }

  ngOnInit() {
    if (this.route.snapshot.queryParams['error']) {
        this.router.navigate(['/user/dashboard']);
    }
    this.requestAccessToken(this.route.snapshot.queryParams['code']);
  }

  requestAccessToken(code: string) {
    // this.zohoService.getAuthToken(code).subscribe( (response: Auth) => {
    //   console.log(response);
    //   this.router.navigate(['/user/connection']);
    // });
  }
}
