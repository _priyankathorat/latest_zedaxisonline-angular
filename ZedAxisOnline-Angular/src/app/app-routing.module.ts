import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AmazonConnectionComponent } from './Connection/amazon-connection/amazon-connection.component';
import { NoAuthenticatedComponent } from './no-authenticated/no-authenticated.component';
import { LoginComponent } from './no-authenticated/login/login.component';
import { ForgotPasswordComponent } from './no-authenticated/forgot-password/forgot-password.component';
import { CreateAccountComponent } from './no-authenticated/create-account/create-account.component';
import { UserAuthenticatedComponent } from './user-authenticated/user-authenticated.component';
import { DashboardHomeComponent } from './user-authenticated/dashboard-home/dashboard-home.component';
import { ConnectionFormComponent } from './user-authenticated/connection-form/connection-form.component';
import { EditProfileComponent } from './user-authenticated/edit-profile/edit-profile.component';
import { ZohoConnectionComponent } from './Connection/zoho-connection/zoho-connection.component';
import { QuickbookSigninComponent } from './no-authenticated/quickbook-signin/quickbook-signin.component';
import { ConnectToQuickbookComponent } from './user-authenticated/connect-to-quickbook/connect-to-quickbook.component';
import { QuickbookConnectComponent } from './user-authenticated/connect-to-quickbook/quickbook-connect/quickbook-connect.component';
import { AuthGuard } from './_guards/auth.guard';
import { ItemSettingComponent } from './user-authenticated/connection-form/item-setting/item-setting.component';

const routes: Routes = [
  {
    path: '', component:  NoAuthenticatedComponent,
    children: [
      { path: '', component: LoginComponent},
      { path: 'forgot-password', component: ForgotPasswordComponent},
      { path: 'create-account', component:  CreateAccountComponent},
      { path: 'quickbook-signin', component: QuickbookSigninComponent}
    ]
  },
  {
    path: '', component: UserAuthenticatedComponent, canActivate: [AuthGuard],
    children: [
      {path: 'dashboard', component: DashboardHomeComponent},
      {path: 'edit-profile', component: EditProfileComponent},
      {path: 'connect-to-quickbook', component: ConnectToQuickbookComponent},
      {path: 'connection', component: ConnectionFormComponent},
      { path: 'zoho-connection', component: ZohoConnectionComponent },
      {path: '', redirectTo: 'dashboard', pathMatch: 'prefix'}
    ]
  },
  { path: 'quickbook-connect', component: QuickbookConnectComponent },
  { path: 'amazon-connection', component:  AmazonConnectionComponent},
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
