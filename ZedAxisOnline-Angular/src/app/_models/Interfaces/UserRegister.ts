export interface UserRegister {
    FirstName: string;
    LastName: string;
    Email: string;
    Password: string;
    ConfirmPassword: string;
    AcceptTC: string;

}
