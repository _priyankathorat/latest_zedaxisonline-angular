import { IdNameString } from './IdName';

export interface QbAccountSettingDropdown {
    itemType: string[];
    taxEntities: IdNameString[];
    cogsAccountEntities: IdNameString[];
    assetAccountEntities: IdNameString[];
    incomeAccountEntities: IdNameString[];
}

export interface QbAccountSettingSubmit {
    type?: string;
    taxCode?: string;
    taxCodeId?: string;
    incomeAccount?: string;
    incomeAccountId?: string;
    cOGSAccount?: string;
    cOGSAccountId?: string;
    assetAccount?: string;
    assetAccountId?: string;
}

