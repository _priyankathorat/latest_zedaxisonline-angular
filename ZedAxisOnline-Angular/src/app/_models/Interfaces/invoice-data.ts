export interface InvoiceData {
    id: number;
    invoice_id: string;
    customer_name: string;
    email: string;
    invoice_date: string;
    currency_code: string;
    total: string;
    status: string;
    billing_address_city: string;
    billing_address_state: string;
    billing_address_zip: string;
    billing_address_country: string;
    shipping_address_street: string;
    shipping_address_city: string;
    shipping_address_state: string;
    shipping_address_zip: string;
    shipping_address_country: string;
}
