export interface MappedField {
    qboTransColumnId?: number;
    ConnectionTransColumnId?: number;
    constantValue?: string;
    selectedFunctionId?: number;


}

export interface ConstantField {
    qboFieldId?: number;
    constantValue?: string;
}

export interface FunctionMappedField {
    qboFieldId?: number;
    functionId?: string;
}
