export interface AmazonResponse {
    AccessToken?: string;
    amazonUserId?: string;
    userId?: number;
}
export interface AllConnectionsResponse {
    connectionTypeId?: number;
    connectionType?: string;
    connectionName?: string;
    id?: number;
}
