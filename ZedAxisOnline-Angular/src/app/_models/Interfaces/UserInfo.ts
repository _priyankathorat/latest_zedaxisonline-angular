export interface UserInfo {
    firstName?: string;
    lastName?: string;
    photoUrl?: string;
    email: string;

    // not confirm of following data
    photoUpload?: FormData;
    role: number;
}
