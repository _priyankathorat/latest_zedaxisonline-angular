export interface MappingGridData {
    id: number;
    qboTransactiontype?: string;
    qboTransColumnName?: string;
    connectionTransColumnId?: FormData;
    isMapped?: string;
    constantValue: string;
    selectedItem: string;
    mappingFunctionId: number;
    sampleData1?: string;
    sampleData2?: string;
}
