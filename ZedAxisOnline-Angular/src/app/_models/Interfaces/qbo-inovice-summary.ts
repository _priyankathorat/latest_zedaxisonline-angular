export interface QboInoviceSummary {
     id: number;
     importType: string;
     isImported: boolean;
     viewUrl: string;
     status: string;
     error: string;
     transactionId: string;
}
