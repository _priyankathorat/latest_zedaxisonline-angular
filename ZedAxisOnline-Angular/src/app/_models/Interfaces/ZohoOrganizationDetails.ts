export interface ZohoOrganizationDetailResponse {
    messsage: string,
   response: ZohoOrganizationDetails;
}

export interface ZohoOrganizationDetails {
    message: string;
    isValid: boolean;
    organizations: organizations[];
}

export interface organizations {
    id: string;
    name: string;
}

export interface SaveConnection {
    authToken: string;
    connectionName: string;
    organizationId: string;
    organizationName: string;
}

export interface ZohoWebHookDetails {
    id: number;
    connectionTypeId: number;
    webhookUrl: string;
}