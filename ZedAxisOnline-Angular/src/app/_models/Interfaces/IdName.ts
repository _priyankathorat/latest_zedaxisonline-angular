export interface IdName {
    id?: number;
    name?: string;
}

export interface IdNameString {
    id: string;
    name: string;
}
