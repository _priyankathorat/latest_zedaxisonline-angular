import { Injectable } from '@angular/core';
declare let alertify: any;
@Injectable({
  providedIn: 'root'
})
export class AlertifyService {
  constructor() {}

  confirm(message: string, okCallback: () => any) {
    alertify.confirm(message, function(e) {
      if (e) {
        okCallback();
      } else {
      }
    });
  }

  dialog(message: string) {
    alertify.dialog(message);
  }

  success(message: string) {
    alertify.success(message).dismissOthers();
  }

  error(message: string) {
    alertify.error(message).dismissOthers();
  }

  warning(message: string) {
    alertify.warning(message);
  }

  message(message: string) {
    alertify.message(message);
  }
}
