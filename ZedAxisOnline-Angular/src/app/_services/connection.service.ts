import { IdName } from './../_models/Interfaces/IdName';
import { QBOTransTypes } from './../_models/Interfaces/QBOTransTypes';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AllConnectionsResponse } from '../_models/Interfaces/AmazonResponse';
import { ConTransTypes } from '../_models/Interfaces/ConTransTypes';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
  apiUrl = environment.ApiUrl;
  paypalUrl = this.apiUrl + 'paypal/';
  baseUrl = this.apiUrl + 'connection/';
  public gridData: any[];
  public transColList: String[];
  public conColList: any[];
  public transSelectedValue: String;
  public conSelectedValue: String;
  public connectionTypeId: number;
  public connectionTransTypeId: number;
  public QBOTransTypeId: number;
  public MappingName: String;
  public amazonUserid: null;
  public ValidMappingName: boolean;
  public paypalUserId: null;
  public ConnectionTypesId: number;
  public ConnectionUserId: string;
  public itemSettingType: string;
  public itemSettingTaxCodeId: number;
  public itemSettingIncomeId: number;
  public itemSettingCogsId: number;
  public itemSettingAssetId: number;
  public FunctionList: Array<IdName> = [
    { name: 'Create Function', id: 1 },
    { name: 'Edit Function', id: 2 }
  ];
  public EditFunctionList: Array<IdName> = [
    { name: 'Select Function', id: 0 }
  ];
  AllConnectionsResponse: AllConnectionsResponse[];
  qboTransTypes: QBOTransTypes[];
  conTransType: ConTransTypes[];
  conTypeId: ConTransTypes;
  // isMappingNameValid: ConTransTypes;
  model: any = {};
  mappingId: number;
  constructor(private http: HttpClient, private router: Router) {}

  PayPalLogin(model: any) {
    return this.http
      .post(this.baseUrl + 'PaypalGetAccessToken', model)
      .pipe(
        map((response: any) => {
          this.paypalUserId = response.connectionDetails.paypalUserId;
          this.router.navigate(['/user/dashboard']);
        })
      );
  }

  AmazonAuth(model: any) {
    console.log(this.model);
    return this.http.post(this.baseUrl + 'AmazonAuth', model).pipe(
      map((response: any) => {
        const url = response;
        if (url) {
          window.location.href = url.authorizationUrl;
        }
      })
    );
  }

  AmazonGetCode(model: any) {
    return this.http.post(this.baseUrl + 'AmazonGetCode', model).pipe(
      map((response: any) => {
        if (response) {
          this.amazonUserid = response.connectionDetails.amazonUserId;
          this.router.navigate(['/user/connection']);
        }
      })
    );
  }

  GetAllConnectionDetails() {
    this.model.userId = localStorage.getItem('userId');
    return this.http
      .post<AllConnectionsResponse[]>(
        this.baseUrl + 'GetAllConnectionDetails',
        this.model
      )
      .pipe(
        map((response: any) => {
          if (response) {
            this.AllConnectionsResponse = response.connectionDetails;
            return this.AllConnectionsResponse;
          }
        })
      );
  }

  GetQBOTransTypes() {
    return this.http
      .get<QBOTransTypes[]>(this.baseUrl + 'GetQBOTransTypes')
      .pipe(
        map((response: any) => {
          return (this.qboTransTypes = response.qboTransTypes);
        })
      );
  }



  GetConnectionTransTypes(model: any) {
    return this.http
      .post<ConTransTypes[]>(this.baseUrl + 'GetConnectionTransTypes', model)
      .pipe(
        map((response: any) => {
          return (this.conTransType = response.conTransTypes);
        })
      );
  }

  GetQBOColumnList(model: any) {
    return this.http.post(this.baseUrl + 'GetQBOColumnList', model).pipe(
      map((response: any) => {
        return (this.transColList = response.qboTranscol);
      })
    );
  }
  GetDataForMappping(model: any) {
    console.log(model);
    return this.http.post(this.baseUrl + 'GetDataForMappping', model).pipe(
      map((response: any) => {
        return (this.gridData = response.gridData);
      })
    );
  }

  GetConColumnList(model: any) {
    return this.http.post(this.baseUrl + 'GetConColumnList', model).pipe(
      map((response: any) => {
        return (this.conColList = response.conTransTypecol);
      })
    );
  }
  CheckMappingName(model: any) {
    return this.http.post(this.baseUrl + 'CheckMappingName', model);
    // .pipe(
    //   map((response: any) => {
    //     return (this.isMappingNameValid = response.isMappingNameValid);
    //   })
    // );
  }


  SaveMappingFunction(model: any) {
    model.userId = localStorage.getItem('userId');
    return this.http.post(this.baseUrl + 'SaveMappingFunction', model).pipe(
      map((response: any) => {
        return (response.functionDetail);
      })
    );
  }

  getQboConnections() {
    return this.http.get(this.baseUrl + 'QuickbookConnection');
  }

  GetFunctionList() {
    this.model = {};
    this.model.userId = localStorage.getItem('userId');
      console.log( this.model.userId);
    return this.http.post<IdName>(this.baseUrl + 'GetMappingFunctionList', this.model).pipe(
      map((response: any) => {
        return (this.EditFunctionList = response.mappingFunctionList);
      })
    );
  }
  GetMappingFunctionData(value: number) {
    this.model = {};
    this.model.Id = value;

    return this.http.post(this.baseUrl + 'GetMappingFunctionData', this.model).pipe(
      map((response: any) => {
        return (response.mappingFunctionDetails);
      })
    );
  }

  UpdateFunctionData(value: any) {
    this.model = {};
    this.model = value;
    return this.http.post(this.baseUrl + 'UpdateFunctionData', this.model).pipe(
      map((response: any) => {
        return (response.functionDetailId);
      })
    );
  }

  DeleteFunctionData(value: any) {
    this.model = {};
    this.model = value;
    return this.http.post(this.baseUrl + 'DeleteFunctionData', this.model).pipe(
      map((response: any) => {
        return (response.isDeleted);
      })
    );
  }



  SaveWebhookEvent(value: any) {
    this.model = {};
    this.model = value;
    return this.http
      .post(this.paypalUrl + 'SaveWebhookEvent', this.model)
      .pipe(
        map((response: any) => {
        })
      );
  }
}
