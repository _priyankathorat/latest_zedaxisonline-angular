import { UserInfo } from './../_models/Interfaces/UserInfo';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiUrl = environment.ApiUrl;
  baseUrl = this.apiUrl + 'auth/';
  ProfilePhotoUrl = '';
  jwthelper = new JwtHelperService();
  decodedToken: any;
  companyName: string;
  connectedToCompany = false;
  userInfo: UserInfo;
  firstNamefromEditProfile: string;
  lastNamefromEditProfile: string;
  photofromEditProfile: string;
  roleFromEditProfile: number;
  modelUserId: any;

  constructor(private http: HttpClient, private router: Router) {}

  login(model: any) {
    return this.http.post(this.baseUrl + 'login', model).pipe(
      map((response: any) => {
        const user = response;
        if (user) {
          this.setTokenAndUserInfoToLocalStorage(user.token);

          console.log(this.decodedToken);
          this.router.navigate(['/user/dashboard']);
        }
      })
    );
  }

  register(model: any) {
    console.log(model);
    return this.http.post(this.baseUrl + 'register', model).pipe(
      map((response: any) => {
        const user = response;
        console.log(user.token);

        if (user) {
          this.setTokenAndUserInfoToLocalStorage(user.token);
          this.router.navigate(['/user/dashboard']);
          console.log(this.decodedToken);
        }
      })
    );
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    if (!token) {
      return false;
    }
    return !this.jwthelper.isTokenExpired(token);
  }
  getProfileInfo1() {
    if (this.loggedIn()) {
      const token = localStorage.getItem('token');
      this.decodedToken = this.jwthelper.decodeToken(token);
      this.modelUserId = +this.decodedToken.nameid;
      this.http.post(this.baseUrl + 'GetuserProfile', this.modelUserId).pipe(
        map((response: any) => {
          const userDetails = response;
                this.userInfo = {
            email: userDetails.email,
            firstName: userDetails.firstName,
            lastName: userDetails.lastName,
            role: +userDetails.role,
            photoUrl: userDetails.photoUrl
          };
        console.log(this.userInfo);

          return this.userInfo;
        })
      ).subscribe(data => {
        console.log('data');

        console.log(data);
      });
    } else {
      this.router.navigate(['/login']);
    }
    return this.userInfo;
  }
  getProfileInfo() {
    if (this.loggedIn()) {
      this.userInfo = {
        email: localStorage.getItem('email'),
        role: +localStorage.getItem('role'),
        firstName: localStorage.getItem('firstName'),
        lastName: localStorage.getItem('lastName'),
      };
      if (localStorage.getItem('ProfilePhotoUrl')) {

        this.userInfo.photoUrl = localStorage.getItem('ProfilePhotoUrl');

      }
      return this.userInfo;
    } else {
      this.router.navigate(['/login']);
    }
    return this.userInfo;

  }

  AppLogOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('QboComapnyName');
    localStorage.removeItem('userId');
    localStorage.removeItem('email');
    localStorage.removeItem('role');
    localStorage.removeItem('firstName');
    localStorage.removeItem('lastName');
    if (localStorage.getItem('ProfilePhotoUrl')) {
    localStorage.removeItem('ProfilePhotoUrl');
    }
    this.router.navigate(['/login']);
  }

  qboLoggedIn(isLoggedin: boolean) {
    // const token = localStorage.getItem('token');

    return isLoggedin;
  }

  qbologin() {
    return this.http.get(this.baseUrl + 'QBOAuth').pipe(
      map((response: any) => {
        const url = response;
        if (url) {
          console.log('RedirectUrl  ' + url.authorizeUrl);
          window.location.href = url.authorizeUrl;
        }
      })
    );
  }

  signInWithIntuit() {
    return this.http.get(this.baseUrl + 'QBOSignIn').pipe(
      map((response: any) => {
        console.log(response);
        const url = response;
        if (url) {
          window.location.href = url.authorizeUrl;
        }
      })
    );
  }

  getQboConnectionStatus() {
    const name = localStorage.getItem('QboComapnyName');
    if (name) {
      this.companyName = name;
      return name;
    }
    return null;
  }

  qboDisconnect() {
    localStorage.removeItem('QboComapnyName');
  }

  qboGetAuthToken(model: any) {
    model.userId =  localStorage.getItem('userId');

    console.log('InSide QboGetAuthToken');
    return this.http.post(this.baseUrl + 'QboGetAuthToken', model).pipe(
      map((response: any) => {
        const Comapany = response;
        if (Comapany) {
          this.companyName = Comapany.companyName;
          this.connectedToCompany = true;
          localStorage.setItem('QboComapnyName', this.companyName);
        }
      })
    );
  }

  qboSignInGetAuthToken(model: any) {
    return this.http.post(this.baseUrl + 'QboSignInGetAuthToken', model).pipe(
      map((response: any) => {
        const user = response;
        if (user) {
          // if (Comapany.companyName) {
          //   this.companyName = Comapany.companyName;
          //   this.connectedToCompany = true;
          //   localStorage.setItem('QboComapnyName', this.companyName);
          // }
          if (user.token) {
            this.setTokenAndUserInfoToLocalStorage(user.token);
            this.router.navigate(['/user/dashboard']);
          }
          // localStorage.setItem('connectedToCompany',  'true');
        }
      })
    );
  }

  setTokenAndUserInfoToLocalStorage(token: any) {
    localStorage.setItem('token', token);
    this.decodedToken = this.jwthelper.decodeToken(token);
    localStorage.setItem('userId', this.decodedToken.nameid);
    localStorage.setItem('email', this.decodedToken.email);
    localStorage.setItem('role', this.decodedToken.role);
    localStorage.setItem('lastName', this.decodedToken.family_name);
    localStorage.setItem('firstName', this.decodedToken.unique_name);
    if (this.decodedToken.certthumbprint) {
    localStorage.setItem('ProfilePhotoUrl', this.ProfilePhotoUrl + this.decodedToken.certthumbprint);
    }
  }
}
