import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ZohoOrganizationDetailResponse, SaveConnection, ZohoWebHookDetails } from '../_models/Interfaces/ZohoOrganizationDetails';
import { AllConnectionsResponse } from '../_models/Interfaces/AmazonResponse';
import { InvoiceData } from '../_models/Interfaces/invoice-data';
import { QboInoviceSummary } from '../_models/Interfaces/qbo-inovice-summary';

@Injectable({
  providedIn: 'root'
})
export class ZohoService {
  baseUrl = environment.ApiUrl;
  isInvoiceLoaded = false;
  selectedConnection: AllConnectionsResponse;
  // invoiceList: InvoiceData;
  callInvoiceList = new EventEmitter<number>();
  constructor(private http: HttpClient, private router: Router) {}

  getOrgnizationDetails(authToken): Observable<ZohoOrganizationDetailResponse> {
    console.log(
      this.baseUrl + 'v1/zoho/OrganizationDetails?authToken=' + authToken
    );
    return this.http.get<ZohoOrganizationDetailResponse>(
      this.baseUrl + 'v1/zoho/OrganizationDetails?authToken=' + authToken
    );
  }

  saveConnection(model: SaveConnection) {
    return this.http.post(this.baseUrl + 'v1/zoho/Connection', model);
  }

  getGuidForWebHook() {
    return this.http.get(this.baseUrl + 'v1/zoho/webhookguid');
  }

  saveWebHook(model: ZohoWebHookDetails) {
    return this.http.post(this.baseUrl + 'v1/zoho/webhook', model);
  }

  getInvoice(connectionId: number, mappingId: number) {
    return this.http.get<InvoiceData>(
      this.baseUrl + 'v1/zoho/invoicelist?' + 'connectionId=' + connectionId  + '&mappingId=' + mappingId
    );
  }

  getImportedSummary(mappingId: number) {
    return this.http.post<QboInoviceSummary>(
      this.baseUrl + 'v1/zoho/ImportToQuickbook?' + 'mappingId=' + mappingId, {mappingId: mappingId}
    );
  }
}


