import { AlertifyService } from './alertify.service';
import { UserInfo } from './../_models/Interfaces/UserInfo';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment.prod';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

const HttpUploadOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' })
};

@Injectable({
    providedIn: 'root'
  })
  export class ProfileService {
    apiUrl = environment.ApiUrl;
    baseUrl = this.apiUrl + 'auth/';
  ProfilePhotoUrl = '';

    constructor(private http: HttpClient, private router: Router, private alertifyService: AlertifyService) {}

    editProfile(userId: number, model: FormData) {
        return this.http.post(this.baseUrl + 'EditProfile', model).pipe(
          map((response: any) => {
            const userdata = response;
            console.log(userdata);

            if (userdata.photoUrl) {

            localStorage.setItem('ProfilePhotoUrl', this.ProfilePhotoUrl + userdata.photoUrl);
            }
         // this.alertifyService.success('Profile updated successfully');
             location.reload();

          })
        );
      }
}
