import { AmazonConnectionComponent } from './Connection/amazon-connection/amazon-connection.component';
import { ProfileService } from './_services/profile.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardContentComponent } from './dashboard-content/dashboard-content.component';
import { HttpClientModule, HttpClient, HttpClientJsonpModule} from '@angular/common/http';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module';
import { ShortenPipe } from './_pipes/shorten-pipe';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from './_service/auth.service';
import { PaypalConnectionComponent } from './Connection/paypal-connection/paypal-connection.component';
import { AlertifyService } from './_services/alertify.service';
import { DynamicScriptLoaderService } from './_services/DynamicScriptLoader.service';
import { GridModule } from '@progress/kendo-angular-grid';
// tslint:disable-next-line:max-line-length
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { NoAuthenticatedComponent } from './no-authenticated/no-authenticated.component';
import { CreateAccountComponent } from './no-authenticated/create-account/create-account.component';
import { LoginComponent } from './no-authenticated/login/login.component';
import { ForgotPasswordComponent } from './no-authenticated/forgot-password/forgot-password.component';
import { UserAuthenticatedComponent } from './user-authenticated/user-authenticated.component';
import { TopMenuComponent } from './user-authenticated/top-menu/top-menu.component';
import { FooterComponent } from './user-authenticated/footer/footer.component';
import { DashboardHomeComponent } from './user-authenticated/dashboard-home/dashboard-home.component';
import { DashboardHomeTableComponent } from './user-authenticated/dashboard-home/dashboard-home-table/dashboard-home-table.component';
import { DayCardComponent } from './user-authenticated/dashboard-home/day-card/day-card.component';
import { EventCardComponent } from './user-authenticated/dashboard-home/event-card/event-card.component';
import { ErrorCardComponent } from './user-authenticated/dashboard-home/error-card/error-card.component';
import { EditService } from './user-authenticated/connection-form/mapping-form/edit.service';
import { MappingFormComponent } from './user-authenticated/connection-form/mapping-form/mapping-form.component';
import {
   FourthStepConnectionComponent } from './user-authenticated/connection-form/fourth-step-connection/fourth-step-connection.component';
import { ConnectionFormComponent } from './user-authenticated/connection-form/connection-form.component';
import { EditProfileComponent } from './user-authenticated/edit-profile/edit-profile.component';
import { ZohoConnectionComponent } from './Connection/zoho-connection/zoho-connection.component';
import { ZohoService } from './_services/zoho.service';
import { environment } from 'src/environments/environment';
import { ZohoConnectionPopupComponent } from './user-authenticated/connection-form/zoho-connection-popup/zoho-connection-popup.component';
import { WebhookPopupComponent } from './user-authenticated/connection-form/webhook-popup/webhook-popup.component';
import {
   EditFunctionPopupComponent } from './user-authenticated/connection-form/mapping-form/edit-function-popup/edit-function-popup.component';
import {
   CreateFunctionPopupComponent
} from './user-authenticated/connection-form/mapping-form/create-function-popup/create-function-popup.component';
import { ImportSummaryPopupComponent } from './user-authenticated/connection-form/import-summary-popup/import-summary-popup.component';
import { QuickbookSigninComponent } from './no-authenticated/quickbook-signin/quickbook-signin.component';
import { ConnectToQuickbookComponent } from './user-authenticated/connect-to-quickbook/connect-to-quickbook.component';
import { QuickbookConnectComponent } from './user-authenticated/connect-to-quickbook/quickbook-connect/quickbook-connect.component';
import { MappingService } from './_service/mapping.service';
import { ItemSettingComponent } from './user-authenticated/connection-form/item-setting/item-setting.component';

export function tokenGetter() {
   return localStorage.getItem('token');
}
@NgModule({
   declarations: [
      AppComponent,
      ShortenPipe,
      CreateAccountComponent,
      LoginComponent,
      FooterComponent,
      TopMenuComponent,
      DashboardContentComponent,
      ForgotPasswordComponent,
      ZohoConnectionComponent,
      EditProfileComponent,
      PaypalConnectionComponent,
      ConnectionFormComponent,
      QuickbookConnectComponent,
      DashboardHomeComponent,
      DashboardHomeTableComponent,
      DayCardComponent,
      EventCardComponent,
      AmazonConnectionComponent,
      FourthStepConnectionComponent,
      ErrorCardComponent,
      MappingFormComponent,
      NoAuthenticatedComponent,
      UserAuthenticatedComponent,
      ZohoConnectionPopupComponent,
      WebhookPopupComponent,
      EditFunctionPopupComponent,
      CreateFunctionPopupComponent,
      ImportSummaryPopupComponent,
      ConnectToQuickbookComponent,
      QuickbookSigninComponent,
      ItemSettingComponent
   ],
   entryComponents: [
      MappingFormComponent,
      EditFunctionPopupComponent,
      CreateFunctionPopupComponent,
      ZohoConnectionPopupComponent,
      WebhookPopupComponent,
      ImportSummaryPopupComponent,
      ItemSettingComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      MatProgressSpinnerModule,
      BrowserAnimationsModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientJsonpModule,
      MaterialModule,
      JwtModule.forRoot({
         config: {
            tokenGetter: tokenGetter,
            whitelistedDomains: environment.jwtWhiteListedDomain,
            blacklistedRoutes : environment.jwtBlackListedRoutes
         }
      }),
      DropDownsModule,
      GridModule,
   ],
   providers: [
      AuthService,
      AlertifyService,
      ProfileService,
      ZohoService,
      MappingService,
      DynamicScriptLoaderService,
      {
         deps: [HttpClient],
         provide: EditService,
         useFactory: (jsonp: HttpClient) => () => new EditService(jsonp)
         }
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
