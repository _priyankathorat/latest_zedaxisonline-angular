import { ConnectionService } from './../_services/connection.service';
import { AuthService } from './../_services/auth.service';
import { Component, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-content',
  templateUrl: './dashboard-content.component.html',
  styleUrls: ['./dashboard-content.component.css']
})
export class DashboardContentComponent implements OnInit {

  @Input() AccountList = [];
  @Input() showEditProfile: boolean;
  @Input() showConnection: boolean;
  @Input() showDashboard: boolean;
  companyName: any;
  connectedToCompany: boolean;
  constructor(private authService: AuthService, private connectionService: ConnectionService) { }

  ngOnInit() {
    console.log(' this.showEditProfile child  ' + this.showEditProfile);

    this.companyName = this.authService.getQboConnectionStatus();
    this.connectedToCompany = this.authService.connectedToCompany;
  }
  qboLogin() {
    this.authService.qbologin().subscribe();
  }

  qboDisconnect() {
    this.authService.qboDisconnect();
    this.companyName = null;
  }
}
