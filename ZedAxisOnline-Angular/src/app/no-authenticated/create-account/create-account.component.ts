import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidationErrors
} from '@angular/forms';

import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { AuthService } from 'src/app/_service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {
  form: FormGroup;
  showLoader = false;
  serverError = false;
  postBackErrorMsg = '';
  constructor(private auth: AuthService, private router: Router) {}

  get firstName() {
    return this.form.get('firstName');
  }

  get lastName() {
    return this.form.get('lastName');
  }

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  get confirmPassword() {
    return this.form.get('confirmPassword');
  }

  get terms() {
    return this.form.get('terms');
  }

  ngOnInit() {
    this.form = new FormGroup(
      {
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [
          Validators.required,
          Validators.minLength(6)
        ]),
        confirmPassword: new FormControl('', Validators.required),
        terms: new FormControl('', Validators.requiredTrue)
      },
      this.passwordMatchValidator
    );
  }

  passwordMatchValidator(g: FormGroup) {
    if (
      g.get('confirmPassword').errors &&
      !g.get('confirmPassword')['missmatch']
    ) {
      return;
    }

    if (g.get('password').value !== g.get('confirmPassword').value) {
      g.get('confirmPassword').setErrors({ missmatch: true });
    } else {
      g.get('confirmPassword').setErrors(null);
    }
    return g.get('password').value === g.get('confirmPassword').value
      ? null
      : { missmatch: true };
  }

  register() {
    this.form.get('terms').markAsDirty();
    this.form.updateValueAndValidity();
    if (this.form.valid) {
      this.showLoader = true;
      this.auth.register(this.form.value).subscribe(
        result => {
          console.log(result);
          if (result) {
            this.router.navigate(['/dashboard']);
          } else {
            console.log(result);
          }
        },
        error => {
          this.showLoader = false;
          this.form.setErrors({postBackError: true});
          this.postBackErrorMsg = error.error;
          console.log(error);
        }
      );
    }
  }
}
