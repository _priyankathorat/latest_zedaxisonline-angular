import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/_service/auth.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  showLoader = false;
  showIntuitLoader = false;
  invalidLogin = false;


  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit() {}

  signIn(credentials: FormGroup) {
    this.showLoader = true;
    this.authService.login(credentials.value)
    .subscribe(result => {
      this.showLoader = false;
      if (result) {
        this.router.navigate(['/dashboard']);
      } else {
        this.invalidLogin = true;
    }
    }, errorResponse => {
       this.showLoader = false;
       this.invalidLogin = true;
       console.log(errorResponse.error);
    });
  }

  getIntuiteSignInUrl() {
    this.showIntuitLoader = true;
    this.authService.getIntuitSignInUrl()
    .subscribe(
      url => (window.location.href = url),
      () => (this.showIntuitLoader = false));
  }
}
