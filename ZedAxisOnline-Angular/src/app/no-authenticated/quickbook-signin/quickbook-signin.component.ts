import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/_service/auth.service';
import { AlertifyService } from 'src/app/_services/alertify.service';

@Component({
  selector: 'app-quickbook-signin',
  templateUrl: './quickbook-signin.component.html',
  styleUrls: ['./quickbook-signin.component.css']
})
export class QuickbookSigninComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router, private alertify: AlertifyService
  ) {}

  ngOnInit() {
    if (this.route.snapshot.queryParams && this.route.snapshot.queryParams['code']) {
      this.authService.signInWithIntuit(this.route.snapshot.queryParams)
      .subscribe(result => {
        if (result) {
          this.router.navigate(['/dashboard']);
        } else {
          this.router.navigate(['']);
          this.alertify.error('Something went wrong.');
      }
      }, errorResponse => {
         console.log(errorResponse.error);
         this.router.navigate(['']);
          this.alertify.error('Something went wrong.');
      });
    } else {
      this.router.navigate(['']);
      this.alertify.error('Not able to connect to intuit.');
    }
  }
}
