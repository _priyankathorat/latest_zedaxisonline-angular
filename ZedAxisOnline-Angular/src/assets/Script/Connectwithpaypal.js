
paypal.use(['login'], function (login) {
  login.render({
    "appid": "AWqRGqd0srgOvJhKRfFkdaBopG5c202KpvKltsqPMCfnjaHyGHKmKLYMh7tAGwrRCo6Ptbi4eOExZMXe",
    "authend": "sandbox",
    "scopes": "openid",
    "containerid": "cwppButton",
    "locale": "en-us",
    "buttonType": "CWP",
    "buttonSize": "lg",
    "returnurl": "http://localhost:4200/paypal-connection"
    // "returnurl": "https://staging.zed-systems.com/paypal-connection"
  });
});