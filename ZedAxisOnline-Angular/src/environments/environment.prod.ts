export const environment = {
  production: true,
  ApiUrl: 'https://staging.zed-systems.com/api/v1/',
  jwtWhiteListedDomain: ['staging.zed-systems.com'],
  jwtBlackListedRoutes: ['staging.zed-systems.com/api/v1/auth']
};
